#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include <string>
#include "libpixyusb2.h"


	
	Pixy2 pixy;

int main(int argc, char **argv)
{

	ros::init(argc, argv, "obstacle");
	ros::NodeHandle n;
    int  Result;
	
		// Initialize Pixy2 Connection //
	{
		Result = pixy.init();
		if (Result < 0) {
			printf ("Error\n");
			printf ("pixy.init() returned %d\n", Result);
			return Result;
		}
		printf ("Success\n");
	}
  
	bool obstacle_inf = false; 
  
	ros::Publisher chatter_pub = n.advertise<std_msgs::Bool>("/obstacle",  1000);

	ros::Rate loop_rate(10);
    std_msgs::Bool obstacle; 

  

  while (ros::ok())
  {

	pixy.ccc.getBlocks();

	if (pixy.ccc.numBlocks) {
	  obstacle_inf = true; 
	} else {
	  obstacle_inf = false; 
	}
	
	obstacle.data = obstacle_inf;

    ROS_INFO("%d", obstacle.data);

    chatter_pub.publish(obstacle);

    ros::spinOnce();

    loop_rate.sleep();
  }


  return 0;
}


