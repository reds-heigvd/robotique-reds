# Marche à suivre



## 1. Installation de l'environnement

#### 1.1 Mise en place de l'OS et wifi

- Flash la carte avec Ubuntu 18.04 LTS : https://ubuntu.com/download/raspberry-pi

- Pour faciliter les manipulations et pour pouvoir effectuer des copier/coller, dans une des partitions de la carte se trouve à la racine le fichier config.txt, ajouter la ligne suivante (permet la connexion UART) : 

  ``` bash
  # serial
  enable_uart=1
  
  # i2c
  dtparam=i2c_arm=on
  ```

- Connexion UART :

  <img src="./img/02_UART_connection.png" alt="02_UART_connection" align="left" style="zoom:60%;" />

- Sur l'ordinateur utilisé pour mettre en place la pi : 

  ``` bash
  sudo picocom -b 115200 /dev/ttyUSB0
  ```

- Mettre la carte dans le pi et se connecter (login = ubuntu, pass = ubuntu)

- Activer le wifi : 

  - ``` bash
    ls /sys/class/net  #(détecte le nom du wifi, wlan0 par exemple)
    ```

  - Modifier le fichier qui se trouve dans le dossier /etc/netplan/<name> en ajoutant le wifi : 

    <img src="./img/01_screen_wifi_set.png" alt="01_screen_wifi_set" align="left" style="zoom:100%;" />

  - ``` bash
    sudo netplan apply
    ```

  - Vérifier si le wifi fonctionne bien avec un ping ou autre.

  

#### 1.2 Changer le layout du clavier

- ``` bash
  sudo nano /etc/default/keyboard
  ```

- Modifier le XKBLAYOUT avec le layout de votre choix (ch pour Suisse, fr pour français etc...)

- Redémarrer le système

  

#### 1.3 Mise en place de ROS sur la pi4

- Installation du système :

  ```bash
  # set up source.list
  sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
  
  # set up les keys
  sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
  
  # debian package à jour
  sudo apt update
  
  # installer ros sans gui. 
  # j'ai installé Melodic mais n'importe lequel fera l'affaire
  sudo apt install ros-melodic-ros-base
  ```

- Mise en place de l'environnement :

  ```bash
  echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
  source ~/.bashrc
  
  # installations utiles et pratiques
  sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
  
  # initialisation de rosdep
  sudo rosdep init
  rosdep update
  ```



#### 1.3 Tests de l'installation de ROS

Il suffit pour tester d'effectuer quelques commandes basiques telles que : 

``` bash
roscore
```

Si roscore ne fonctionne pas, ajouter dans le bashrc la ligne suivante : 

```bash
export ROS_HOSTNAME=localhost
```

Source le bashrc et relancer roscore.

Ne pas hésiter à tester des packages de base de ROS.



#### 1.4 Mise en place de l'i2c

- Ajouter dans le fichier config.txt la ligne suivante : 

  ``` 
  dtparam=i2c_arm=on
  ```

- Il est important d'installer les outils nécessaires pour utiliser l'i2c : 

  ```bash
  sudo apt-get install libi2c-dev
  sudo apt-get install i2c-tools   
  ```

- ![03_i2c_connect](./img/03_i2c_connect.png)

  Voici une image montrant comment câbler l'i2c sur la rpi4.

  Et voici le cablage sur la board i2cPWM : 

  ![04_i2c_onboard_PWM](./img/04_i2c_onboard_PWM.png)

- Enfin la dernière étape est de modifier les droits de l'i2c : 

  ``` bash
  sudo chmod 777 /dev/i2c-1
  ```





## 2. Développement de l'application de base du robot

Pour commencer, il est nécessaire de créer le répertoire de travail qui aura la structure suivante (catkin) : 

​             racine

   |             |           |        

build        src      devel

Pour obtenir cette structure, créer un racine du projet avec un dossier nommé "src" à l'intérieur, puis à la racine du dossier (où se trouve le dossier src) taper la commande suivante : 

``` bash
catkin_make
```

La structure précédemment décrite devrait être générée.



#### 2.1 Contrôle du module i2c PWM pour l'envoie des commandes au moteur

- ``` bash
  git clone https://gitlab.com/bradanlane/ros-i2cpwmboard
  ```

- Ajouter dans /ros-i2cpwmboard/src/*.c, dans les includes

  ``` c
  extern "C" {
      #include <linux/i2c.h>
      #include <linux/i2c-dev.h>
      #include <i2c/smbus.h>
  }
  ```

  Eventuellement modifier la fréquence (60 par exemple)

- Ajouter dans /ros-i2cpwmboard/CMakelists.txt

  ``` bash
  target_link_libraries(i2cpwm_board ${catkin_LIBRARIES} -li2c)
  ```

  Ajout du link avec la librairie libi2c installée pour l'i2c.

- A la racine du dépôt : 

  ``` bash
  catkin_make 
  source devel/setup.bash
  ```

- Vérifier avec rosrun que tout fonctionne correctement

  ```bash
  # premier terminal
  roscore
  
  # deuxième terminal
  rosrun i2cpwm_board i2cpwm_board
  
  # troisième terminal
  rostopic list # permet de voir si les topics ont bien été créés
  rostopic pub /servos_absolute i2cpwm_board/ServoArray # finir avec tab la commande
  ```

  

## 3. Set up du contrôleur (ds4)

Pour que le ds4 fonctionne avec le rpi4 : 

- Installations à faire : 

  ```bash
  sudo apt-get install python3-pip 
  sudo pip3 install ds4drv # driver pour la manette ps4 sur Linux
  ```

- Récupérer le package ROS "joy". Pour ce faire deux solutions possibles :

  - Installation via apt 

    ```bash
    sudo apt-get install ros-melodic-joy # remplacer Melodic par la bonne version ROS
    ```

  - Récupération du package depuis le git : https://github.com/ros-drivers/joystick_drivers et mettre le dossier "joy" dans les sources du répertoire de travail Catkin créé et enfin refaire les manipulations pour le build du projet.

  La seconde méthode est vivement conseillé car la première ne fonctionne pas forcément toujours (raisons inconnues...).

- Connexion du bluetooth. Pour connecter la manette à la rpi4 il faut activer le service bluetooth. Dans le cas de la rpi4, il est indispensable d'attacher un UART série à la pile bluetooth en tant qu'interface de transport HCI. La commande pour le faire : 

  ```bash
  sudo hciattach /dev/ttyAMA0 bcm43xx 921600
  ```

- Si l'outil hciattach n'est pas disponible, il faut installer bluez : 

  ```bash
  sudo apt-get install bluez
  ```

- Pour éviter tout problème, une fois l'attach effectué, un redémarrage du service bluetooth est recommendé : 

  ```bash
  sudo systemctl restart bluetooth
  ```

- A présent il est possible de lancer le driver précédemment installé (ds4drv) : 

  ```bash
  sudo ds4drv # dans un autre terminal ou avec &
  ```

- Pour lier la manette, appuyer longtemps sur le bouton "share" et le bouton "PS" simultanément. Le driver devrait au bout de quelques secondes produire un message de ce type : 

  ![05_ds4_message_success](./img/05_ds4_message_success.png)

  Si ce message apparaît, bonne nouvelle tout s'est bien passé.

- Dans /dev/input/ un nouveau fichier de type "jsX" devrait être présent. Si aucun autre contrôleur n'est connecté, alors le fichier /dev/input/js0 doit être présent. Pour y accéder depuis ROS, il est nécessaire de changer les droits : 

  ```bash
  sudo chmod 777 /dev/inpput/js0
  ```

- Lancer le node joy et confirmer que le système fonctionne.

  





















